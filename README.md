# Minds HTTP Proxy

This service is used to proxy http to the public internet.

## Installing

`helm3 install http-proxy . -n http-proxy`
